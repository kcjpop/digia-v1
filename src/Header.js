import React from 'react';

const Header = () => {
  return (
    <div className="Header">
      <div className="header-container">
        <a href="#" className="site-title-container">
          <span className="logo" />
          <span className="site-title">Mi'Lord Software</span>
        </a>
      </div>
    </div>
  );
};

export default Header;
