import React from 'react';

const Table = props => {
  return (
    <div className="Table">
      <table className="table-of-participants">
        <tr className="table-header-row">
          <th onClick={props.sortByName}>Name</th>
          <th onClick={props.sortByEmail}>Email address</th>
          <th onClick={props.sortByPhone}>Phone number</th>
        </tr>
        {props.participants.map(participant => {
          return participant.id !== props.editedParticipantId ? (
            <tr className="table-body-row">
              <td>{participant.name}</td>
              <td>{participant.email}</td>
              <td>{participant.phone}</td>
              <td onClick={props.prepareParticipantEditing(participant)}>
                <i class="fas fa-pen"></i>
              </td>
              <td onClick={() => props.deleteParticipant(participant.id)}>
                <i class="fas fa-trash"></i>
              </td>
            </tr>
          ) : (
            <tr className="table-body-row">
              <td>
                <input
                  onChange={props.handleEditedNameChange}
                  value={props.editedName}
                />
              </td>
              <td>
                <input
                  onChange={props.handleEditedEmailChange}
                  value={props.editedEmail}
                />
              </td>
              <td>
                <input
                  onChange={props.handleEditedPhoneChange}
                  value={props.editedPhone}
                />
              </td>
              <td onClick={props.handleEditedParticipantSubmit}>Save</td>
            </tr>
          );
        })}
      </table>
    </div>
  );
};

export default Table;
