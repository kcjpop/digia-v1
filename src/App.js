import React, { Component } from 'react';
import './App.css';
import Table from './Table';
import Form from './Form';
import Header from './Header';

class App extends Component {
  state = {
    participants: [
      {
        id: '6939937510',
        name: 'Karen Bardsley',
        email: 'karen@example.com',
        phone: '0401415926',
      },
      {
        id: '3421170679',
        name: 'Millie Bright',
        email: 'millie@example.com',
        phone: '0405820974',
      },
      {
        id: '5359408128',
        name: 'Lucy Bronze',
        email: 'lucy@example.com',
        phone: '0408214808',
      },
      {
        id: '5493038196',
        name: 'Alex Greenwood',
        email: 'alex@example.com',
        phone: '0404811174',
      },
      {
        id: '2712019091',
        name: 'Steph Houghton',
        email: 'steph@example.com',
        phone: '0404428810',
      },
      {
        id: '0249141273',
        name: 'Demi Stokes',
        email: 'demi@example.com',
        phone: '0404564856',
      },
      {
        id: '9171536436',
        name: 'Karen Carney',
        email: 'karen@example.com',
        phone: '0407245870',
      },
      {
        id: '9415116094',
        name: 'Jade Moore',
        email: 'jade@example.com',
        phone: '0407892590',
      },
      {
        id: '3105118548',
        name: 'Jill Scott',
        email: 'jill@example.com',
        phone: '0403305727',
      },
      {
        id: '8301194912',
        name: 'Jodie Taylor',
        email: 'jodie@example.com',
        phone: '0400744623',
      },
      {
        id: '1907021798',
        name: 'Jordan Pickford',
        email: 'jordan@example.com',
        phone: '0409833673',
      },
      {
        id: '7669405132',
        name: 'Kyle Walker',
        email: 'kyle@example.com',
        phone: '0406094370',
      },
      {
        id: '7363717872',
        name: 'John Stones',
        email: 'john@example.com',
        phone: '0400005681',
      },
      {
        id: '6892589235',
        name: 'Lewis Dunk',
        email: 'lewis@example.com',
        phone: '0401468440',
      },
      {
        id: '4771309960',
        name: 'Eric Dier',
        email: 'eric@example.com',
        phone: '0404201995',
      },
      {
        id: '1609631859',
        name: 'Dele Ali',
        email: 'dele@example.com',
        phone: '0405187072',
      },
      {
        id: '2619311881',
        name: 'Jesse Lingard',
        email: 'jesse@example.com',
        phone: '0405024459',
      },
      {
        id: '7669147303',
        name: 'Harry Kane',
        email: 'harry@example.com',
        phone: '0407101000',
      },
      {
        id: '9375195778',
        name: 'Marcus Rashford',
        email: 'marcus@example.com',
        phone: '0405982534',
      },
      {
        id: '2164201989',
        name: 'Raheem Sterling',
        email: 'raheem@example.com',
        phone: '0401857780',
      },
    ],
    inputtedName: '',
    inputtedEmail: '',
    inputtedPhone: '',
    namesAreAsc: true,
    emailsAreAsc: true,
    phonesAreAsc: true,
    editedParticipantId: '',
    editedName: '',
    editedEmail: '',
    editedPhone: '',
  };

  handleInputtedNameChange = event => {
    this.setState({
      inputtedName: event.target.value,
    });
  };

  handleInputtedEmailChange = event => {
    this.setState({
      inputtedEmail: event.target.value,
    });
  };

  handleInputtedPhoneChange = event => {
    this.setState({
      inputtedPhone: event.target.value,
    });
  };

  handleParticipantFormSubmit = event => {
    event.preventDefault();
    const newParticipant = {
      name: this.state.inputtedName,
      email: this.state.inputtedEmail,
      phone: this.state.inputtedPhone,
    };
    const participants = [...this.state.participants, newParticipant];
    this.setState({
      participants,
    });
  };

  deleteParticipant = participantId => {
    const participants = this.state.participants.filter(
      participant => participant.id !== participantId
    );
    this.setState({
      participants,
    });
  };

  sortByName = () => {
    const sortedParticipants = [...this.state.participants].sort((a, b) =>
      this.state.namesAreAsc
        ? a.name > b.name
          ? 1
          : -1
        : a.name > b.name
        ? -1
        : 1
    );
    this.setState({
      participants: sortedParticipants,
      namesAreAsc: !this.state.namesAreAsc,
    });
  };

  sortByEmail = () => {
    const sortedParticipants = [...this.state.participants].sort((a, b) =>
      this.state.emailsAreAsc
        ? a.email > b.email
          ? 1
          : -1
        : a.email > b.email
        ? -1
        : 1
    );
    this.setState({
      participants: sortedParticipants,
      emailsAreAsc: !this.state.emailsAreAsc,
    });
  };

  sortByPhone = () => {
    const sortedParticipants = [...this.state.participants].sort((a, b) =>
      this.state.phonesAreAsc
        ? a.phone > b.phone
          ? 1
          : -1
        : a.phone > b.phone
        ? -1
        : 1
    );
    this.setState({
      participants: sortedParticipants,
      phonesAreAsc: !this.state.phonesAreAsc,
    });
  };

  prepareParticipantEditing = participant => {
    const { id, name, email, phone } = participant;
    return () => {
      return this.setState({
        editedParticipantId: id,
        editedName: name,
        editedEmail: email,
        editedPhone: phone,
      });
    };
  };

  handleEditedNameChange = event => {
    this.setState({
      editedName: event.target.value,
    });
  };

  handleEditedEmailChange = event => {
    this.setState({
      editedEmail: event.target.value,
    });
  };

  handleEditedPhoneChange = event => {
    this.setState({
      editedPhone: event.target.value,
    });
  };

  handleEditedParticipantSubmit = () => {
    const editedParticipant = this.state.participants.find(
      participant => participant.id === this.state.editedParticipantId
    );
    editedParticipant.name = this.state.editedName;
    editedParticipant.email = this.state.editedEmail;
    editedParticipant.phone = this.state.editedPhone;
    const participants = this.state.participants.map(participant => {
      return participant.id !== this.state.editedParticipantId
        ? participant
        : editedParticipant;
    });
    this.setState({
      participants,
      editedParticipantId: '',
    });
  };

  render() {
    return (
      <div className="App">
        <Header />
        <div className="main">
          <div className="main-container">
            <h1 className="page-title">List of participants</h1>
            <Form
              inputtedName={this.state.inputtedName}
              inputtedEmail={this.state.inputtedEmail}
              inputtedPhone={this.state.inputtedPhone}
              handleInputtedNameChange={this.handleInputtedNameChange}
              handleInputtedEmailChange={this.handleInputtedEmailChange}
              handleInputtedPhoneChange={this.handleInputtedPhoneChange}
              handleParticipantFormSubmit={this.handleParticipantFormSubmit}
            />
            <Table
              participants={this.state.participants}
              deleteParticipant={this.deleteParticipant}
              sortByName={this.sortByName}
              sortByEmail={this.sortByEmail}
              sortByPhone={this.sortByPhone}
              prepareParticipantEditing={this.prepareParticipantEditing}
              editedParticipantId={this.state.editedParticipantId}
              handleEditedNameChange={this.handleEditedNameChange}
              handleEditedEmailChange={this.handleEditedEmailChange}
              handleEditedPhoneChange={this.handleEditedPhoneChange}
              editedName={this.state.editedName}
              editedEmail={this.state.editedEmail}
              editedPhone={this.state.editedPhone}
              handleEditedParticipantSubmit={this.handleEditedParticipantSubmit}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
