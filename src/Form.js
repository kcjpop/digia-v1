import React from 'react';

const Form = props => {
  return (
    <div className="form-component">
      <form
        onSubmit={props.handleParticipantFormSubmit}
        className="add-participant-form"
      >
        <label for="name" />
        <input
          type="text"
          value={props.inputtedName}
          onChange={props.handleInputtedNameChange}
          id="name"
          name="name"
          placeholder="Full name"
        />
        <label for="email" />
        <input
          type="email"
          value={props.inputtedEmail}
          onChange={props.handleInputtedEmailChange}
          id="email"
          name="email"
          placeholder="Email address"
        />
        <label for="tel" />
        <input
          type="tel"
          value={props.inputtedPhone}
          onChange={props.handleInputtedPhoneChange}
          id="tel"
          name="tel"
          placeholder="Phone number"
        />
        <div className='flex-grow'></div>
        <button>Add new</button>
      </form>
    </div>
  );
};

export default Form;
